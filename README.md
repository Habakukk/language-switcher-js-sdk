If you don't want to use URL mapping to automatically translate pages, you need to give users a way to manually change languages. This simple widget allows users to translate the page by simply selecting a language from the menu. 

In the Javascript snippet below, we have provided some common configuration options. If these options don't meet your needs, you should change the HTML/CSS/JS source code as needed.
